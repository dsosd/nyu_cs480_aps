# Overview

This repository is a collection of test cases for the problem sets and recitations in the Spring 2021 APS course.

| Week | Type | Problem 1 | Problem 2 | Problem 3 | Problem 4 | Problem 5 | Misc. Problems |
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|1|Problem set|[Add 2](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps01/add_2/test)|[Fib numbers](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps01/fibonacci/test)|[Fib numbers 2](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps01/fibonacci_2/test)|[Car Value](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps01/car/test)|[Rod Sculpture](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps01/rod/test)|[Add](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps01/add/test)|
|2|Problem set|[Ayu's Wall](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps02/wall/test)|[Big Little Endian](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps02/endian/test)|[Negative Base](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps02/negative/test)|[Base Conversion](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps02/conversion/test)|[Chess Championship](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps02/chess/test)||
|3|Problem set|[Labor Strike](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps03/strike/test)|[Task Planning](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps03/scheduling/test)|[Awesome Number](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps03/prime/test)|[Train](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps03/train/test)|[Stack Puzzle](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps03/permutation/test)||
|4|Problem set|[Albert's Dream](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps04/dictionary/test)|[Reverse Polish Notation](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps04/postfix/test)|[Prime Gap](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps04/prime/test)|[Bitwise Or](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps04/or/test)|[Ferry](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps04/ferry/test)||
|5|Problem set|[Buggy Keyboard](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps05/keyboard/test)|[Queuing With Friends](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps05/friends/test)|[Sia's Box](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps05/mystery/test)|[Daily Prize](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps05/prize/test)|[Unique Subarray](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps05/subarray/test)||
|6|Problem set|[Giant Squad](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps06/median/test)|[Grade Sort](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps06/grading/test)|[Calvin's Stars](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps06/stars/test)|[Virus](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps06/doctor/test)|[Command Center](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps06/network/test)||
|7|Problem set|[Ayu's Candies](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps07/candy/test)|[Poppi's Rocket](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps07/rocket/test)|[APS Homework](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps07/homework/test)|[Move Union Find](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps07/disunion/test)|[Math Class](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps07/largest/test)||
|8|Problem set|[Pocket Money](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps08/change/test)|[Ruthless War](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps08/alive/test)|[Efficient Adding](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps08/add/test)|[Find Sums](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps08/multisum/test)|[Sprinklers](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps08/coverage/test)||
|9|Problem set|[Prime Pair](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps09/prime/test)|[No Change](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps09/change/test)|[Yet Another Sorting Problem](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps09/flip/test)|[CIMS Printer](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps09/printer/test)|[Board Game](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/ps09/game/test)||
|1|Recitation|[A+B Problem](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/recit01/a_a_plus_b/test)|[Balanced Brackets](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/recit01/b_brackets/test)|[Splitting Numbers](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/recit01/c_split/test)|[K-th Smallest Number](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/recit01/d_smallest/test)|||
|2|Recitation|[Binary Numbers](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/recit02/a_consecutive/test)|[Xor Segment](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/recit02/b_xor/test)|[Equalize](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/recit02/c_match/test)||||
|3|Recitation|[Generalized Matrioshkas](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/recit03/a_general/test)|[Integer Lists](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/recit03/b_rainbow/test)|[Knapsack 1](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/recit03/c_knapsack/test)|[Queues: A Tale of Two Stacks](https://github.com/dsosd/nyu_cs480_aps/tree/feature/test_cases/recit03/d_queue/test)|||

# Progress

This is my progress through the problems listed above. Ideally, these stats will be updated every week.

| Set | Problem | Test cases (pass/fail) | CPU Timing (min/avg/max) | Solved | Language |
|:-:|:-:|:-:|:-:|:-:|:-:|
|PS01|ps01/add|2/0|0.03/0.04/0.04|Yes|C++|
|PS01|ps01/add_2|2/0|0.03/0.03/0.03|Yes|C++|
|PS01|ps01/car|6/0|0.04/0.05/0.06|Yes|C++|
|PS01|ps01/fibonacci|2/0|0.03/0.03/0.03|Yes|C++|
|PS01|ps01/fibonacci_2|2/0|0.04/0.04/0.04|Yes|C++|
|PS01|ps01/rod|3/0|0.05/0.06/0.06|Yes|C++|
|PS02|ps02/chess|3/0|0.03/0.03/0.04|Yes|C++|
|PS02|ps02/conversion|4/0|0.03/0.04/0.05|Yes|C++|
|PS02|ps02/endian|4/0|0.03/0.04/0.04|Yes|C++|
|PS02|ps02/negative|5/0|0.04/0.04/0.04|Yes|C++|
|PS02|ps02/wall|1/0|0.04/0.04/0.04|Yes|C++|
|PS03|ps03/permutation|8/0|0.07/0.57/3.93|Yes|C++|
|PS03|ps03/prime|4/0|0.04/317.88/1271.4|Yes|C++|
|PS03|ps03/scheduling|7/0|0.07/12.62/47.16|Yes|C++|
|PS03|ps03/strike|3/0|0.04/0.22/0.57|Yes|C++|
|PS03|ps03/train|5/0|0.04/1.33/6.43|Yes|C++|
|PS04|ps04/dictionary|3/0|0.05/0.11/0.23|Yes|C++|
|PS04|ps04/ferry|5/0|0.04/0.04/0.06|Yes|C++|
|PS04|ps04/or|8/0|0.03/0.04/0.06|Yes|C++|
|PS04|ps04/postfix|4/0|0.04/0.04/0.05|Yes|C++|
|PS04|ps04/prime|6/0|14.41/26.0/48.04|Yes|C++|
|PS05|ps05/friends|2/0|2.67/2.67/2.67|Yes|C++|
|PS05|ps05/keyboard|3/0|0.36/0.37/0.37|Yes|C++|
|PS05|ps05/mystery|5/0|0.04/0.05/0.07|Yes|C++|
|PS05|ps05/prize|5/0|0.05/0.06/0.08|Yes|C++|
|PS05|ps05/subarray|4/0|0.06/0.06/0.07|Yes|C++|
|PS06|ps06/doctor|3/0|0.04/0.05/0.08|Yes|C++|
|PS06|ps06/grading|2/0|0.1/0.11/0.12|Yes|C++|
|PS06|ps06/median|2/0|0.03/0.04/0.04|Yes|C++|
|PS06|ps06/network|3/0|0.04/0.05/0.05|Yes|C++|
|PS06|ps06/stars|2/0|0.05/0.05/0.05|Yes|C++|
|PS07|ps07/candy|4/0|0.05/0.05/0.06|Yes|C++|
|PS07|ps07/disunion|6/0|0.04/13.93/83.33|Yes|C++|
|PS07|ps07/homework|2/0|0.03/0.04/0.04|Yes|C++|
|PS07|ps07/largest|11/0|0.04/0.09/0.53|Yes|C++|
|PS07|ps07/rocket|4/0|0.05/0.05/0.05|Yes|C++|
|PS08|ps08/add|8/0|0.03/0.04/0.05|Yes|C++|
|PS08|ps08/alive|6/0|0.04/14.43/51.46|Yes|C++|
|PS08|ps08/change|2/0|0.04/0.06/0.07|Yes|C++|
|PS08|ps08/coverage|6/0|0.04/0.04/0.05|Yes|C++|
|PS08|ps08/multisum|3/0|0.15/0.53/1.19|Yes|C++|
|PSEUDO_MIDTERM|pseudo_midterm/alarm|1/0|0.04/0.04/0.04|Yes|C++|
|PSEUDO_MIDTERM|pseudo_midterm/knapsack|6/0|0.03/0.16/0.74|Yes|C++|
|PSEUDO_MIDTERM|pseudo_midterm/network|2/0|0.05/0.05/0.05|Yes|C++|
|PSEUDO_MIDTERM|pseudo_midterm/prime|4/0|16.59/17.6/18.81|Yes|C++|
|MIDTERM|midterm/bitstep|1/0|0.04/0.04/0.04|Yes|C++|
|MIDTERM|midterm/cluster|2/0|0.04/0.04/0.05|Yes|C++|
|MIDTERM|midterm/dictionary|1/0|0.06/0.06/0.06|Yes|C++|
|MIDTERM|midterm/wall|1/0|0.03/0.03/0.03|Yes|C++|
